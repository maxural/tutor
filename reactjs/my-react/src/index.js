import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import JSXexam from './jsx_exem';
import './index.css';

const myFirst = <h1>Test the item</h1>

const sandy =(
    <div>
        <p>SANDY elements</p>
    </div>
);
ReactDOM.render(myFirst, document.getElementById('root'));
ReactDOM.render(sandy, document.getElementById('sandy'));
ReactDOM.render(<JSXexam />, document.getElementById('jsx-sample'));