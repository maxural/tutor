import React, { Component } from 'react';
class JSXexam extends  Component{

    render (){
        const createEl = React.createElement('h1', {}, 'I use JSX');
        return (
            <div className="JSXexam">
                <h2>JSX exam</h2>
                <h3>It was made with JSX</h3>
              <createEl />
            </div>
        );
    }
}
export default JSXexam;