// introduction ES6
//class
class Car {
    constructor(name, model) {
        this.brand = name;
    }

    present() {
        return 'I ve a ' + this.brand;
    }
}

mycar = new Car("Ford");
console.log("mycar present", mycar.present());

//class inheritance
class Model extends Car {
    constructor(name, mod) {
        super(name);
        this.model = mod;
    }

    show() {
        return this.present() + ',  it is a '+ this.model
    }
}
mycar = new Model("Ford", "Mustang");
console.log(mycar.show());


hello = function () {
    return "This text for return";
};
console.log(hello());
// array function
lamba = ()=>{
    return "This text of Lamba";
};
console.log(lamba());
// array functions return value by Default
hello = ()=>"Array func return";
console.log(hello());
// array func without parentheses
hello = val =>"Variable without Parentheses"+val;
console.log(hello());
console.log("THE VARABLE");
// var variable
var myvar;
myvar="TEST VAR";
// let variable
let mylet;
mylet ="TEST LET ";
const constVar="IT IS CONST";
// const variable

function testvar() {
    var func = "TEST FUNC";
    console.log(mylet);
    console.log(myvar);
    if(true){
        let insideLet;
        insideLet="Inside let";
        console.log(insideLet);
    console.log("const", constVar);
    }
    //console.log("outside scope", insideLet);
    console.log("inside func",func);

}
//console.log(func);
testvar();
