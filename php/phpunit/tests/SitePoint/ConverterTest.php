<?php


namespace SitePoint\Converter;
use PHPUnit\Framework\TestCase;

class ConverterTest extends TestCase
{
    /***
     * the first test
     */
    public function testHello(){
    $this->assertEquals('Hello','Hell'.'o','the message has');

    }
/**
 * the second test
 */
    public function testSimpleConversion(){
    $input ='{"key":"value","key2":"value2"}';
        $output =["key"=>"value","key2"=>"value2"];
        $converter = new \SitePoint\Converter\Converter();
        $this->assertEquals($output, $converter->convertString($input));
}
}