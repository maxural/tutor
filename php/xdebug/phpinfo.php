<?php
//phpinfo();
function test($param){
    echo "hidden element";
    return $param;
}

class Main{
    private $param;
    public function __construct($param)
    {
        $this->param = $param;
    }

    public function test(){
       return test($this->param);
    }
}

$prop = new Main("hello class");
echo $prop->test();